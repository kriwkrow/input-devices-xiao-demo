#include<Wire.h>

#define HALL_PIN D2
#define TLE493D_ADDR 0x35

void setup() {
  Serial.begin(19200);

  Wire.begin();
  Wire.setClock(400000);

  // reset TLE493D

  Wire.beginTransmission(TLE493D_ADDR);
  Wire.write(0xFF);
  Wire.endTransmission();
  
  Wire.beginTransmission(TLE493D_ADDR);
  Wire.write(0xFF);
  Wire.endTransmission();
  
  Wire.beginTransmission(TLE493D_ADDR);
  Wire.write(0x00);
  Wire.endTransmission();
  
  Wire.beginTransmission(TLE493D_ADDR);
  Wire.write(0x00);
  Wire.endTransmission();
  
  delayMicroseconds(50);
  
  // configure TLE493D
  
  Wire.beginTransmission(TLE493D_ADDR);
  Wire.write(0x10);
  Wire.write(0x28);
  
  // config register 0x10
  // ADC trigger on read after register 0x05
  // short-range sensitivity
  
  Wire.write(0x15);
  Wire.endTransmission();
}

void loop() {
  int hallReading = analogRead(HALL_PIN);

  // read data
  
  Wire.requestFrom(TLE493D_ADDR,6);

  // each axis value is two bytes so it seems

  uint8_t v0,v1,v2,v3,v4,v5;
  v0 = Wire.read();
  v1 = Wire.read();
  v2 = Wire.read();
  v3 = Wire.read();
  v4 = Wire.read();
  v5 = Wire.read();

  // here we do some bit math

  int x = (v0 << 4) + ((v4 & 0xF0) >> 4);
  int y = (v1 << 4) + (v4 & 0x0F);
  int z = (v2 << 4) + (v5 & 0x0F);
  
  if (x > 2047) {
    x = x - 4096;
  }
  
  if (y > 2047) {
    y = y - 4096;
  }
  
  if (z > 2047){
    z = z - 4096;
  }
  
  // send data
  
  Serial.print("x:");
  Serial.print(x);
  Serial.print(",");
  
  Serial.print("y:");
  Serial.print(y);
  Serial.print(",");
  
  Serial.print("z:");
  Serial.print(z);
  Serial.print(",");
  
  Serial.print("hall:");
  Serial.println(hallReading);

  delay(20);
}
