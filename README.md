# Input Devices XIAO Demo

This has been made with the intention to cover the Input Devices week group assingment of the Fab Academy. 

![Hero Shot](Images/InputDevices_XIAO_Demo_HeroShot.jpg)

## Features

- [Allegro A1324](https://www.allegromicro.com/~/media/Files/Datasheets/A1324-5-6-Datasheet.ashx) Hall-Effect sensor
- [Infineon TLE493D](https://media.digikey.com/pdf/Data%20Sheets/Infineon%20PDFs/TLE493D-A2B6_V1.3_4-9-19.pdf) 3-axis Hall-Effect sensor
- [QWIIC](https://www.sparkfun.com/qwiic) connection for a variety of I2C boards

## Demoing

The idea is to showcase analog sensor reading capability using the  Allegro A1324 analog hall-effect sensor, I2C usage basics with the Infineon TLE493D 3-axis hall-effect sensor, and then show extended I2C capabilities using the QWIIC connection.

The board has been tested with the SparkFun [SEN-18993 Distance Sensor](https://www.sparkfun.com/products/18993), thus the example code in the `Arduino` directory reflects that.

## Arduino

The `Arduino` directory contains code examples that have been tested with the XIAO RP2040 microcontroller module. Make sure that you have installed the needed libraries.

You can learn more on how to get started with XIAO RP2040 on the [XIAO RP2040 Getting Started](https://wiki.seeedstudio.com/XIAO-RP2040/) page.

For the SparkFun Distance Sensor you will need to install the [SparkFun VL53L1X Arduino library](https://github.com/sparkfun/SparkFun_VL53L1X_Arduino_Library).

## License 

This code is public domain but you buy me a beer if you use this and we meet someday (Beerware license).

The code examples use snippets from source code written by [Neil Gershenfeld](https://academy.cba.mit.edu/classes/input_devices/mag/TLE493D/hello.TLE493D.t412.ino) and [Nathan Seidle](https://github.com/sparkfun/SparkFun_VL53L1X_Arduino_Library/blob/master/examples/Example1_ReadDistance/Example1_ReadDistance.ino).